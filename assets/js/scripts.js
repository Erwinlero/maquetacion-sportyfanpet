(function() {
    
    "use strict";

  $(".btn-nav-xs").click(function(e) {
      e.preventDefault();
      $("#bg-darken").toggleClass("active");
  });


  // Toggle the side navigation
  $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    };
  });


//Scroll to top
	$(document).ready(function() {
	  $(window).scroll(function() {
	    if ($(this).scrollTop() > 50) {
	      $('#back-to-top').fadeIn();
	    } else {
	      $('#back-to-top').fadeOut();
	    }
	  });
	  // scroll body to 0px on click
	  $('#back-to-top').click(function() {
	    $('body,html').animate({
	      scrollTop: 0
	    }, 400);
	    return false;
	  });
	});







/*Slider testimonials*/
       /*  var swiper = new Swiper(".swiper", {
          slidesPerView:1,
          spaceBetween: 10,
          pagination: {
            el: ".js-swiper-single-testimonials-pagination",
            clickable: true,
          },
          navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
          },
          breakpoints: {
            320: {
              slidesPerView: 2,
              spaceBetween: 50,
            },
            440: {
              slidesPerView: 4,
              spaceBetween: 50,
            },
            640: {
              slidesPerView: 5,
              spaceBetween: 50,
            },
            768: {
              slidesPerView: 6,
              spaceBetween: 50,
            },
            1024: {
              slidesPerView: 8,
              spaceBetween: 80,
            },
          },
        }); */


})();

/* slider benefit */
var swiper = new Swiper('.swiper', {
 
  direction: getDirection(),
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  breakpoints: {
    672: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    1070: {
      slidesPerView: 3,
      spaceBetween: 20,
    },
    1350: {
        slidesPerView: 4,
        spaceBetween: 20,
    },
    
},
  on: {
    resize: function () {
      swiper.changeDirection(getDirection());
    },
  },
});

/* end slider benefit */

/* slider teams */
var swiper = new Swiper('.swiper-teams', {
 
  direction: getDirection(),
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  breakpoints: {
    320: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    570: {
      slidesPerView: 4,
      spaceBetween: 20,
    },
    770: {
      slidesPerView: 5,
      spaceBetween: 20,
    },
    1070: {
      slidesPerView: 6,
      spaceBetween: 20,
    },
    1350: {
        slidesPerView: 11,
        spaceBetween: 20,
    },
    
},
  on: {
    resize: function () {
      swiper.changeDirection(getDirection());
    },
  },
});
/* end slider teams */

function getDirection() {
  var windowWidth = window.innerWidth;
  var direction = window.innerWidth <= 760 ? 'horizontal' : 'horizontal';

  return direction;
}

  /* scrolled */
  var navbar = document.querySelector('nav')

  window.onscroll = function() {
    console.log (window.pageXOffset)
    /* pageYOffset or scrollY */
    if (window.pageYOffset > 10) {
      navbar.classList.add('scrolled')
    }
    else  {
    navbar.classList.remove('scrolled')
  }
  }